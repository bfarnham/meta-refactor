/* © Copyright CERN, 2015.  All rights not expressly granted are reserved.
 * meta.cpp
 *
 *  Created on: Aug 18, 2015
 * 	Author: Benjamin Farnham <benjamin.farnham@cern.ch>
 *      Author: Piotr Nikiel <piotr@nikiel.info>
 *
 *  This file is part of Quasar.
 *
 *  Quasar is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public Licence as published by
 *  the Free Software Foundation, either version 3 of the Licence.
 *
 *  Quasar is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public Licence for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with Quasar.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <meta.h>
#include <LogIt.h>
#include <ConfigurationDecorationUtils.h>
#include <ASStandardMetaData.h>
#include <ASBuildInformation.h>
#include <ASQuasar.h>
#include <ASServer.h>
#include <ASSourceVariableThreadPool.h>
#include "MetaBuildInfo.h"
#include "QuasarVersion.h"
#include "metaBackwardsCompatibilityUtils.h"

using std::begin;
using std::end;


void initializeBuildInformation(AddressSpace::ASNodeManager *nm)
{
    auto buildInformation = Meta::findStandardMetaDataChildObject<AddressSpace::ASBuildInformation>(nm, "BuildInformation");
    LOG(Log::INF) << __FUNCTION__ << " found StandardMetaData object ["<<std::hex<<buildInformation<<"]. Populating...";

    buildInformation->setBuildHost(BuildMetaInfo::getBuildHost().c_str(), OpcUa_Good);
    buildInformation->setBuildTimestamp(BuildMetaInfo::getBuildTime().c_str(), OpcUa_Good);
    buildInformation->setCommitID(BuildMetaInfo::getCommitID().c_str(), OpcUa_Good);
    buildInformation->setToolkitLibs(BuildMetaInfo::getToolkitLibs().c_str(), OpcUa_Good);
}

void intializeQuasar(AddressSpace::ASNodeManager *nm)
{
    auto quasar = Meta::findStandardMetaDataChildObject<AddressSpace::ASQuasar>(nm, "Quasar");
    LOG(Log::INF) << __FUNCTION__ << " found StandardMetaData object ["<<std::hex<<quasar<<"]. Populating...";

    quasar->setVersion(QUASAR_VERSION_STR, OpcUa_Good);
}

void initializeServer(AddressSpace::ASNodeManager *nm)
{
    auto server = Meta::findStandardMetaDataChildObject<AddressSpace::ASServer>(nm, "Server");
    LOG(Log::INF) << __FUNCTION__ << " found StandardMetaData object ["<<std::hex<<server<<"]. Populating...";

    server->setRemainingCertificateValidity("uninitialized", OpcUa_Good);
}

void Meta::initializeMeta(AddressSpace::ASNodeManager *nm)
{
    LOG(Log::INF) << __FUNCTION__ << " called";
 
    initializeBuildInformation(nm);
    intializeQuasar(nm);
    initializeServer(nm);
}

Configuration::StandardMetaData& getStandardMetaData(Configuration::Configuration & standardMetaDataParent)
{
    if(standardMetaDataParent.StandardMetaData().empty())
    {
        LOG(Log::INF) << __FUNCTION__ << " parent does not contain a StandardMetaData element; adding one";
        Configuration::StandardMetaData standardMetaDataConfig;
        Configuration::DecorationUtils::push_back(
            standardMetaDataParent, 
            standardMetaDataParent.StandardMetaData(), 
            standardMetaDataConfig, 
            Configuration::Configuration::StandardMetaData_id);
    }
    return standardMetaDataParent.StandardMetaData().front();
}

Configuration::Log& getLog(Configuration::StandardMetaData& logParent)
{
    if(logParent.Log().empty())
    {
        LOG(Log::INF) << __FUNCTION__ << " parent does not contain a Log element; adding one";
        Configuration::Log logConfig;
        Configuration::DecorationUtils::push_back(
            logParent, 
            logParent.Log(), 
            logConfig, 
            Configuration::StandardMetaData::Log_id);        
    }
    return logParent.Log().front();
}

Configuration::SourceVariableThreadPool& getSourceVariableThreadPool(Configuration::StandardMetaData& sourceVariableThreadPoolParent)
{
    if(sourceVariableThreadPoolParent.SourceVariableThreadPool().empty())
    {
        LOG(Log::INF) << __FUNCTION__ << " parent does not contain a SourceVariableThreadPool element; adding one";
        Configuration::SourceVariableThreadPool sourceVariableThreadPoolConfig("10", "1");
        Configuration::DecorationUtils::push_back(
            sourceVariableThreadPoolParent, 
            sourceVariableThreadPoolParent.SourceVariableThreadPool(), 
            sourceVariableThreadPoolConfig,
            Configuration::StandardMetaData::SourceVariableThreadPool_id);                
    }
    return sourceVariableThreadPoolParent.SourceVariableThreadPool().front();
}

Configuration::LogLevel& getGeneralLogLevel(Configuration::Log& generalLogLevelParent)
{
    if(generalLogLevelParent.LogLevel().empty())
    {
        const Log::LOG_LEVEL level = Log::getNonComponentLogLevel();
        LOG(Log::INF) << __FUNCTION__ << " parent does not contain a GeneralLogLevel element; adding one";
        Configuration::LogLevel logLevelConfig("GeneralLogLevel", Log::logLevelToString(level));
        Configuration::DecorationUtils::push_back(
            generalLogLevelParent, 
            generalLogLevelParent.LogLevel(), 
            logLevelConfig,
            Configuration::Log::LogLevel_id);        
    }
    return generalLogLevelParent.LogLevel().front();
}

Configuration::ComponentLogLevels& getComponentLogLevels(Configuration::Log& componentLogLevelsParent)
{
    if(componentLogLevelsParent.ComponentLogLevels().empty())
    {
        LOG(Log::INF) << __FUNCTION__ << " parent does not contain a ComponentLogLevels element; adding one";
        Configuration::ComponentLogLevels componentLogLevelsConfig;
        Configuration::DecorationUtils::push_back(
            componentLogLevelsParent,
            componentLogLevelsParent.ComponentLogLevels(),
            componentLogLevelsConfig,
            Configuration::Log::ComponentLogLevels_id);
    }
    return componentLogLevelsParent.ComponentLogLevels().front();
}

Configuration::LogLevel& getComponentLogLevel(const std::string& name, const Log::LogComponentHandle& componentHandle, Configuration::ComponentLogLevels& componentLogLevelParent)
{
    const auto matchLogLevelByNameFn = [name](Configuration::LogLevel& logLevel) { return name == logLevel.name(); };

	// find level in configuration
    auto pos = std::find_if(begin(componentLogLevelParent.LogLevel()), end(componentLogLevelParent.LogLevel()), matchLogLevelByNameFn);
    if(pos != end(componentLogLevelParent.LogLevel())) return *pos;

    // not found - add. First retrieve component log level (as string)
    Log::LOG_LEVEL level;
    if(!Log::getComponentLogLevel(componentHandle, level))
    {
        LOG(Log::ERR) << __FUNCTION__ << " programming error - unknown logging component name ["<<name<<"], handle ["<<componentHandle<<"]. Defaulting to [INF]";
        level = Log::INF;
    }

    LOG(Log::INF) << __FUNCTION__ << " LogLevel element name ["<<name<<"] not found in ComponentLogLevels, will create and add to config";
    Configuration::LogLevel logLevelConfig(name, Log::logLevelToString(level));

    Configuration::DecorationUtils::push_back(
        componentLogLevelParent,
        componentLogLevelParent.LogLevel(),
        logLevelConfig,
        Configuration::ComponentLogLevels::LogLevel_id);

    // should find it now
    pos = std::find_if(begin(componentLogLevelParent.LogLevel()), end(componentLogLevelParent.LogLevel()), matchLogLevelByNameFn);
    if(pos != end(componentLogLevelParent.LogLevel())) return *pos;    

    // otherwise error
    std::ostringstream err;
    err << __FUNCTION__ << " programming error - failed to find LogLevel with name ["<<name<<"] in ComponentLogLevels";
    throw std::runtime_error(err.str());
}

void validateComponentLogLevels( const Configuration::ComponentLogLevels& componentLogLevels )
{
	std::list<std::string> checkedComponentNames;

	for( const Configuration::LogLevel& logLevelConfig: componentLogLevels.LogLevel() )
	{
		std::string name(logLevelConfig.name());
		// check for duplicates
		if (std::find(begin(checkedComponentNames), end(checkedComponentNames), name) != end(checkedComponentNames))
		{
			std::ostringstream err;
			err << __FUNCTION__ << " Configuration contains ComponentLogLevel name [" << name << "] more than once: mis-configuration.";
            LOG(Log::ERR) << err.str() << " Throwing exception";
			throw std::runtime_error(err.str());
		}
		checkedComponentNames.push_back( name );
	}
}

void configureComponentLogLevels(Configuration::Log& componentLogLevelsParent)
{
    auto& componentLogLevels = getComponentLogLevels(componentLogLevelsParent);
    Meta::BackwardsCompatibilityUtils::convertOldComponentLogLevel(componentLogLevels);
    validateComponentLogLevels(componentLogLevels);

    // Loop round only those logging components registered with LogIt. 
    // Config _may_ contain other named logging components
    // But if LogIt doesn't know them - can only ignore them here.
    // Perhaps the unknown components will be registered later on 
    //
    // Fine: e.g. some shared libray, which registers its own logging 
    // component, might be dynamically loaded later or something.
    for(const auto& registeredComponent : Log::getComponentLogsList())
    {
        const Log::LogComponentHandle& componentHandle = registeredComponent.first;
        const std::string componentName = Log::getComponentName(componentHandle);

        auto& logLevelConfig = getComponentLogLevel(componentName, componentHandle, componentLogLevels);

        // parse config logLevel string to a LogIt log level.
        const std::string levelString(logLevelConfig.logLevel());
        Log::LOG_LEVEL level;
        if(!Log::logLevelFromString(levelString, level))
        {
            LOG(Log::WRN) << __FUNCTION__ << " failed to parse logLevel string ["<<levelString<<"] to valid level ID for logging component [nm:"<<componentName<<", hdl:"<<componentHandle<<"]. Ignoring";
            continue;
        }

        // set component (should not fail)
        if(Log::setComponentLogLevel(componentHandle, level))
        {
            LOG(Log::INF) << __FUNCTION__ << " set logging component [nm:"<<componentName<<", hdl:"<<componentHandle<<"] to threshold ["<<levelString<<"]";
        }
        else
        {
            LOG(Log::ERR) << __FUNCTION__ << " programming error: failed to set logging component [nm:"<<componentName<<", hdl:"<<componentHandle<<"] to threshold ["<<levelString<<"]";
        }
    }
}

void configureGeneralLogLevel(Configuration::Log& generalLogLevelParent)
{
    Meta::BackwardsCompatibilityUtils::convertOldGeneralLogLevel(generalLogLevelParent);

    auto& generalLogLevel = getGeneralLogLevel(generalLogLevelParent);
    Log::LOG_LEVEL level;
    if(Log::logLevelFromString(generalLogLevel.logLevel(), level))
    {
        Log::setNonComponentLogLevel(level);
        LOG(Log::INF) << __FUNCTION__ << " set GeneralLogLevel to threshold ["<<Log::logLevelToString(level)<<"]";
    }
}

void configureLog(Configuration::StandardMetaData& logParent)
{
    auto& log = getLog(logParent);
    configureGeneralLogLevel(log);
    configureComponentLogLevels(log);
}

void configureSourceVariableThreadPool(Configuration::StandardMetaData& sourceVariableThreadPoolParent)
{
    getSourceVariableThreadPool(sourceVariableThreadPoolParent);
}

void configureStandardMetaData(Configuration::Configuration & standardMetaDataParent)
{
    auto& standardMetaData = getStandardMetaData(standardMetaDataParent);
    configureLog(standardMetaData);
    configureSourceVariableThreadPool(standardMetaData);
}

void Meta::configureMeta(Configuration::Configuration & config, AddressSpace::ASNodeManager *nm, UaNodeId parentNodeId)
{
    LOG(Log::INF) << __FUNCTION__ << " called";

    configureStandardMetaData(config);
}

void Meta::destroyMeta (AddressSpace::ASNodeManager *nm){}