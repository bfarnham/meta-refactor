#pragma once
#include <LogIt.h>
namespace CustomStuff
{
    extern Log::LogComponentHandle ArseComponent;
    extern Log::LogComponentHandle ElbowComponent;
    extern Log::LogComponentHandle LaterzComponent; // will be registered after server initialisation; via OPC-UA write to DUserClass
}; // namespace CustomStuff