# @author bfarnham
# @date 07-Sep-2015
# The purpose of this file is to set parameters for building the CAEN OPC-UA server on EN/ICE machines.

# The approach is to satisfy the requirements as much as possible.
set (CMAKE_CXX_STANDARD 11) # works for CMake v3.1+
message("using build configuration from enice_sl6_configuration.cmake. Shared libs supported [${TARGET_SUPPORTS_SHARED_LIBS}]")
message(STATUS "environment vars: BOOST_HOME [$ENV{BOOST_HOME}] UNIFIED_AUTOMATION_HOME [$ENV{UNIFIED_AUTOMATION_HOME}]")

#-------
#Boost
#-------
SET(IGNORE_DEFAULT_BOOST_SETUP ON)
include( boost_system_cc7.cmake )

#------
#OPCUA
#------
add_definitions( -DBACKEND_UATOOLKIT )
if( NOT DEFINED ENV{UNIFIED_AUTOMATION_HOME} )
	message(FATAL_ERROR "environment variable UNIFIED_AUTOMATION_HOME not defined - please set this to a 64bit unified automation toolkit installation")
else()
	SET( OPCUA_TOOLKIT_PATH $ENV{UNIFIED_AUTOMATION_HOME} )		
endif()
message(STATUS "UA TOOLKIT - OPC-UA toolkit path [${OPCUA_TOOLKIT_PATH}]" )

include_directories (
	${OPCUA_TOOLKIT_PATH}/include/uastack
	${OPCUA_TOOLKIT_PATH}/include/uabasecpp
 	${OPCUA_TOOLKIT_PATH}/include/uaservercpp
	${OPCUA_TOOLKIT_PATH}/include/xmlparsercpp
	${OPCUA_TOOLKIT_PATH}/include/uapkicpp
)
link_directories( ${OPCUA_TOOLKIT_PATH}/lib )

SET( OPCUA_TOOLKIT_LIBS_DEBUG   "-luamoduled -lcoremoduled -luapkicppd -luabasecppd -luastackd -lxmlparsercppd -lxml2 -lssl -lcrypto -lpthread" )
SET( OPCUA_TOOLKIT_LIBS_RELEASE "-luamodule  -lcoremodule  -luapkicpp  -luabasecpp  -luastack  -lxmlparsercpp  -lxml2 -lssl -lcrypto -lpthread" )
add_custom_target( quasar_opcua_backend_is_ready )

#-----
# LogIt
#-----
set(LOGIT_BACKEND_STDOUTLOG ON CACHE BOOL "The basic back-end: logs to stdout")
set(LOGIT_BACKEND_BOOSTLOG ON CACHE BOOL "Rotating file logger back-end: fixed size on disk based on boost logging library")
set(LOGIT_BACKEND_UATRACE OFF CACHE BOOL "UnifiedAutomation toolkit logger")
set(LOGIT_BACKEND_WINDOWS_DEBUGGER OFF CACHE BOOL "Windows debugger logger")
MESSAGE( STATUS "LogIt build options: stdout [${LOGIT_BACKEND_STDOUTLOG}] boost [${LOGIT_BACKEND_BOOSTLOG}] uaTrace [${LOGIT_BACKEND_UATRACE}] windows debugger [${LOGIT_BACKEND_WINDOWS_DEBUGGER}]" )

#-----
#XML Libs
#-----
#As of 03-Sep-2015 I see no FindXerces or whatever in our Cmake 2.8 installation, so no find_package can be user...
# TODO perhaps also take it from environment if requested
SET( XML_LIBS "-lxerces-c" ) 

# TODO: split between Win / MSVC, perhaps MSVC has different notation for these
add_definitions(-Wall -Wno-deprecated -std=gnu++0x -Wno-literal-suffix -DUNIX)
